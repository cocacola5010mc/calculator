# Genreates the breakpoints for brackets in a string
# Type String -> Array<Array<String>>
def generateBreakpoints(string):
    breakpoints = []
    for letterIndex in range(0, len(string)):
        if string[letterIndex] == "(":
            bracketCounter = 0
            broken = False
            for subLetterIndex in range(letterIndex + 1, len(string)):
                if not broken:
                    if string[subLetterIndex] == '(':
                        bracketCounter += 1
                    elif string[subLetterIndex] == ')':
                        if bracketCounter == 0:
                            breakpoints.append([letterIndex, subLetterIndex])
                            broken = True  # I wonder if there was a feature in
                            # python that let me do this more gracefully!!!!
                            # You saying that using break is cheating is
                            # rubbish, all break is, is a jump to the next part
                            # of the code. Its a feature of the language, and
                            # a useful one at that.
                        else:
                            bracketCounter -= 1
    return breakpoints


# Finds which breakpoints are inside one another
# Type Array<Array<String>> -> Array<Array<String>>
def findOrder(breakpoints):
    orders = []
    for breakpointIndex in range(0, len(breakpoints)):
        if breakpointIndex == 0:
            orders.append(0)
        else:
            searching = True
            searchIndex = breakpointIndex - 1
            while searching:
                if searchIndex == -1:  # IE, we havn't found a breakpoint that
                    # it belongs to
                    orders.append(0)
                    searching = False  # I totally agree that using break here
                    # would be wrong
                elif breakpoints[breakpointIndex][0] > breakpoints[searchIndex][0] and breakpoints[breakpointIndex][1] < breakpoints[searchIndex][1]:
                    orders.append(orders[searchIndex] + 1)
                    searching = False
                else:
                    searchIndex -= 1
    return orders


# Generates an array of string which indecate the brackets.
# Type String -> Array <String>
def parseBrackets(string):
    breakpoints = generateBreakpoints(string)
    orders = findOrder(breakpoints)
    if len(breakpoints) == 0:
        return [string]
    else:
        arrayRepresentation = [string]
        positionInString = 0
        for pointIndex in range(0, len(breakpoints)):
            if orders[pointIndex] == 0:
                part = arrayRepresentation
                repLength = len(part) - 1
                prefix = part[repLength][0:breakpoints[pointIndex][0] - positionInString]
                segment = part[repLength][breakpoints[pointIndex][0] - positionInString + 1:breakpoints[pointIndex][1] - positionInString]
                suffix = part[repLength][breakpoints[pointIndex][1] - positionInString + 1:len(part[repLength])]
                arrayRepresentation[len(arrayRepresentation) - 1] = prefix
                arrayRepresentation.append([segment])
                arrayRepresentation.append(suffix)
                positionInString = breakpoints[pointIndex][1] + 1
            elif orders[pointIndex] == 1:
                part = arrayRepresentation[len(arrayRepresentation) - 2]
                repLength = len(part) - 1
                positionInString -= 1
                prefix = part[repLength][0:breakpoints[pointIndex][0] - positionInString]
                segment = part[repLength][breakpoints[pointIndex][0] - positionInString + 1:breakpoints[pointIndex][1] - positionInString]
                suffix = part[repLength][breakpoints[pointIndex][1] - positionInString + 1:len(part[repLength])]
                arrayRepresentation[len(arrayRepresentation) - 2][repLength] = prefix
                arrayRepresentation[len(arrayRepresentation) - 2].append([segment])
                arrayRepresentation[len(arrayRepresentation) - 2].append(suffix)
                positionInString = breakpoints[pointIndex][1] + 1
    return arrayRepresentation
