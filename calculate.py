import bracketParser as bp


# Searches forward in a string to find a float
def findNum(string, pos):
    spaceCount = 0
    stringNum = ''
    for letterIndex in range(pos + 1, len(string)):
        if string[letterIndex] == ' ':
            spaceCount += 1
            if spaceCount == 2:
                return [float(stringNum), letterIndex]
        else:
            stringNum += string[letterIndex]


# Searches back in a string to find a float
def findNumBack(string, pos):
    spaceCount = 0
    stringNum = ''
    for letterIndex in range(pos - 2, -1, -1):
        if string[letterIndex] == ' ':
            spaceCount += 1
            if spaceCount == 1:
                return [float(stringNum), letterIndex]
        else:
            stringNum = string[letterIndex] + stringNum


# Takes in a string and calculats all of one type of operation
def calculateComponent(string, component, operation):
    searchString = string
    searching = True
    while searching:
        pos = searchString.find(component)
        if pos == -1:
            searching = False
        else:
            [arg1, cutStart] = findNumBack(searchString, pos)  # Finds x of x op y
            [arg2, cutEnd] = findNum(searchString, pos)  # Finds y of x op y
            searchString = searchString[0:cutStart] + ' ' + str(operation(arg1, arg2)) + searchString[cutEnd:len(searchString)]
    return searchString


# We have a string like "3 + 5 * 4 * 2 - 1 * 3"
def calculateString(string):
    searchString = string
    if searchString[0] != ' ':  # these ifs correctly pad the experssion for the calculate component function
        searchString = ' ' + searchString
    if searchString[-1] != ' ':
        searchString += ' '
    searchString = calculateComponent(searchString, '/', lambda a, b: a / b)  # these calculate each opperation
    searchString = calculateComponent(searchString, '*', lambda a, b: a * b)
    searchString = calculateComponent(searchString, '-', lambda a, b: a - b)
    searchString = calculateComponent(searchString, '+', lambda a, b: a + b)
    return str(float(searchString))  # removes any extra string


# Concatinates, Flattens, and Calculates a string
def conflatinate(array):
    combined = ''
    for item in array:
        if type(item) == list:
            combined += conflatinate(item)
        else:
            combined += item
    return calculateString(combined)


# Gets the array representation and calculates the answer
def calculate(string):
    arrayRep = bp.parseBrackets(string)
    answerString = conflatinate(arrayRep)
    return float(answerString)
