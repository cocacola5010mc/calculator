import bracketParser as bp


def test(message, function, params, expected):
    print(message)
    result = function(*params)
    if result == expected:
        print('Was successful')
    else:
        print('Failed')
        print('Expected', expected)
        print('Got', result)


test('Testing that breakpoint generation is correct with one set of brackets', bp.generateBreakpoints, ['3 + (3 + 2)'], [[4, 10]])
test('Testing that breakpoint generation is correct with two sets of brackets', bp.generateBreakpoints, ['3 + (3 + 2) + (9 + 7)'], [[4, 10], [14, 20]])
test('Testing that breakpoint generation is correct with order 1 brackets', bp.generateBreakpoints, ['3 + (3 + (3 + 1))'], [[4, 16], [9, 15]])

test('Finding order of one breakpoint', bp.findOrder, [[[4, 10]]], [0])
test('Finding order of two breakpoints', bp.findOrder, [[[4, 10], [14, 20]]], [0, 0])
test('Finding order of two intersecting breakpoints', bp.findOrder, [[[4, 16], [9, 15]]], [0, 1])

test('Parsing string with one set of brackets', bp.parseBrackets, ['3 + (3 + 2)'], ['3 + ', ['3 + 2'], ''])
test('Parsing string with two sets of brackets', bp.parseBrackets, ['3 + (3 + 2) + (9 + 7)'], ['3 + ', ['3 + 2'], ' + ', ['9 + 7'], ''])
test('Parsing with order 1 brackets', bp.parseBrackets, ['3 + (3 + (3 + 1))'], ['3 + ', ['3 + ', ['3 + 2'], ''], ''])
